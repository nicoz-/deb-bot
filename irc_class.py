import socket
import sys
import time
from msg_parser import MsgParser


_LAST_PING_MAX_SECONDS=180
_LAST_RESPONSE_MAX_SECONDS=480

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class BotError(Error):
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

class IRC:

    def __init__(self,family,logger):
        # Define the socket
        self.irc = socket.socket()
        self.irc = socket.socket(family, socket.SOCK_STREAM)
        self.timeout=5
        self.logger=logger
        self.last_pong_replay =""
        self.last_ping=time.time()
        self.last_response = time.time()

    def settimeout(self,timeout):
        tmot=max(5,timeout)
        tmot=min(tmot,max(_LAST_PING_MAX_SECONDS-(time.time()-self.last_ping),1))
        self.timeout=tmot


    def send(self, channel, msg):
        # Transfer data
        msg_sent="PRIVMSG " + channel + " :" + msg + "\n"
        # self.irc.send(bytes("PRIVMSG " + channel + " :" + msg + "\n", "UTF-8"))
        self.irc.send(bytes(msg_sent, "UTF-8"))
        self.logger.info(("--> "+msg_sent).strip())
    
    def send_op(self, channel, nick_rec):
        mode = "MODE " + channel + " +o " + nick_rec + "\n"
        self.irc.send(bytes(mode, "UTF-8"))
        self.logger.info(("--> "+mode).strip())
    
    def send_deop(self, channel, nick_rec):
        mode = "MODE " + channel + " -o " + nick_rec + "\n"
        self.irc.send(bytes(mode, "UTF-8"))
        self.logger.info(("--> "+mode).strip())

    def kick_chan(self, channel, nick_rec):
        #mode = "PRIVMSG " + channel + " :" +"KICK " + channel + " " + nickrec +"\n"
        mode = "KICK " + channel + " " + nick_rec +"\n"
        self.irc.send(bytes(mode, "UTF-8"))
        self.logger.info(("--> "+mode).strip)

    def kickban_chan(self, channel, nick_rec):
        mode = "MODE " + channel + " +b " + nick_rec +"\n"
        self.irc.send(bytes(mode, "UTF-8"))
        self.logger.info(("--> "+mode).strip)
        time.sleep(1)
        mode = "KICK " + channel + " " + nick_rec +"\n"
        self.irc.send(bytes(mode, "UTF-8"))
        self.logger.info(("--> "+mode).strip)


    def connect(self, server, port, channel, botnick, botnickpass, botpass ):
        # Connect to the server
        #print("Connecting to: " + server)
        self.logger.info("Connecting to: " + server)
        self.irc.connect((server, port))

        # Perform user authentication
        self.irc.send(bytes("USER " + botnick + " " + botnick + " " + botnick + " :python debbot for debian-it\n", "UTF-8"))
        self.irc.send(bytes("NICK " + botnick + "\n", "UTF-8"))


        if botnickpass != "":
            self.irc.send(bytes("NICKSERV IDENTIFY " + botnickpass + " " + botpass + "\n", "UTF-8"))
        # waiting for IRC Cloak
            time.sleep(10)
        time.sleep(5)

        # join the channel
        self.irc.send(bytes("JOIN " + channel + "\n", "UTF-8"))


    def get_response(self):
        time.sleep(1)
        # Get the response
        self.logger.info("Waiting for response")
        self.irc.settimeout(self.timeout)
        try:
            resp = self.irc.recv(2040).decode("UTF-8")
            if not resp:
                raise BotError('BotError1','BotError1: Empty socket response. Stopping bot...')
            self.logger.info(("<-- " + resp).strip())
            self.last_response = time.time()
            parsed_msg=MsgParser(resp)
            # index =resp.find('PING')
            # if index != -1:# and index <15:
            if parsed_msg.command=='PING':
                msg1= ':'+parsed_msg.args[0]
                self.pong(msg1)
                # msg='PONG ' + msg1+ '\r\n'
                # self.irc.send(bytes(msg, "UTF-8"))
                # self.logger.info(("--> " + msg).strip())
        except socket.timeout as e:
            resp="Timeout: "+str(e)
            parsed_msg = MsgParser(resp)
            self.logger.info(resp)
            self.time_out_handler()
        except socket.error as e:
            msg=f"BotError2: Socket error: {e}. Stopping bot..."
            self.logger.error(msg)
            raise BotError("BotError2", msg)
        except BotError as e:
            self.logger.error(e.message)
            raise e
            # raise BotError("BotError2", f"BotError2: Socket error {e}. Stopping bot...")
        return parsed_msg

    def time_out_handler(self):
        if time.time() - self.last_response > _LAST_RESPONSE_MAX_SECONDS:
            self.logger.error("No response for too long. The bot may be disconnected. Stopping bot...")
            raise BotError('BotError3', 'BotError3: No response for too long. The bot may be disconnected. Stopping bot...')
        # I check last ping and if more than _LAST_PING_MAX_SECONDS seconds have passed
        # I behave as if I have missed it
        if time.time() - self.last_ping > _LAST_PING_MAX_SECONDS:
            self.logger.info("Suspect missed ping")
            self.pong("")

    def pong(self,pong_reply):
        pr=pong_reply
        if not pr:
            # I probably missed some pings and try to fix it
            self.logger.info("Try to fix missed ping")
            pr=self.last_pong_replay

        msg = 'PONG ' + pr + '\r\n'
        self.irc.send(bytes(msg, "UTF-8"))
        self.logger.info(("--> " + msg+" ").strip())
        self.last_pong_replay=pr
        self.last_ping=time.time()

    def info_nick(self, nick):
        # Transfer data
        msg_sent="USERHOST " + nick + "\n"
        # self.irc.send(bytes("PRIVMSG " + channel + " :" + msg + "\n", "UTF-8"))
        self.irc.send(bytes(msg_sent, "UTF-8"))
        self.logger.info(("Request nick info --> "+msg_sent).strip())
        time.sleep(1)
        resp = self.irc.recv(2040).decode("UTF-8")
        self.logger.info(("Nick info <-- " + resp).strip())
        parsed_msg = MsgParser(resp)
        result=parsed_msg.get_ip_nick_userhost()
        return result
