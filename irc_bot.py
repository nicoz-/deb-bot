#!/usr/bin/env python3

# This bot reads news from a couple of sources
# I found here the basic bot code: https://www.techbeamers.com/create-python-irc-bot/
# on this I have built my news reader

from irc_class import *
import os,time
import datetime
import getfeeds ,irccommands,ipcommand, pckg
import configparser
import argparse
import subprocess
import urllib, re
import json
import logging, logging.handlers
from msg_parser import MsgParser

def starts_with(text,start):
    l_start=len(start)
    start_text=text.strip()[:l_start]
    result=start_text==start
    return result


def ends_with(text,end):
    l_end=len(end)
    end_text=text.strip()[-l_end:]
    result=end_text==end
    return result


def print_feeds(channel,feeds):
    logger.info("print_feeds")
    for feed in reversed(feeds):
        irc.send(channel, feed)
        time.sleep(1)

def check_news():
    logger.info("check_news")
    s=dsa.get_new_feeds()
    print_feeds(channel, s)
    s=planet.get_new_feeds()
    print_feeds(channel, s)

def get_logger(fn):
    logger = logging.getLogger('debbot')
    logger.setLevel(logging.DEBUG)
    # handler = logging.handlers.SysLogHandler(address='/dev/log')
    # handler = logging.FileHandler(fn)
    # formatter = logging.Formatter('%(asctime)s - %(name)s: %(levelname)s - %(message)s')
    # handler.setFormatter(formatter)
    # logger.addHandler(handler)
    # handler = logging.handlers.TimedRotatingFileHandler(fn, 'D', 1, 7)
    handler = logging.handlers.TimedRotatingFileHandler(fn, when='d', interval=1, backupCount=7)
    formatter = logging.Formatter('%(asctime)s - %(name)s: %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger

def ip_command(arg):
    if arg==botnick:
        #hide bot info
        return
    result=ipcommand.check_ip_command(arg)
    if not result:
        arg1=irc.info_nick(arg)
        if arg1:
            result = ipcommand.check_ip_command(arg1)
            if result:
                result.insert(0,f'Nick: \002{arg}\002 ; Host: \002{arg1}\002')
    if result:
        for x in result:  
            irc.send(channel, x) 
    else:
       irc.send(channel, "unknown host/nick")


def get_url_title(url):
    try:
        response = urllib.request.urlopen(url)
        html = response.read().decode('utf-8')
        title = re.search(r'<title>(.*?)</title>', html, re.IGNORECASE)
        if title:
            return title.group(1)
        else:
            return None
    except:
        return None


def get_bot_uptime():
    now = datetime.datetime.utcnow()
    delta = now - bot_connection_time
    hours, remainder = divmod(int(delta.total_seconds()), 3600)
    minutes, seconds = divmod(remainder, 60)
    days, hours = divmod(hours, 24)


    if days:
        fmt = '{d} days, {h} hours, {m} minutes, and {s} seconds'
    else:
        fmt = '{h} hours, {m} minutes, and {s} seconds'

    return fmt.format(d=days, h=hours, m=minutes, s=seconds)



def dpkg_command(arg):
    result=pckg.get_info_package(arg)
    for x in result:
        irc.send(channel, x.replace("\n", " "))



#start Commands for raspberry

def get_temperature():
    temp = subprocess.run(["vcgencmd","measure_temp"],stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8")
    sub_temp = temp.stdout[:5]
    return str(sub_temp)

def get_kernel():
    kernel = subprocess.run(["uname","-a"],stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8")
    sub_kernel = kernel.stdout
    return str(sub_kernel)

def get_os():
    ossys = subprocess.run(["cat","/etc/issue"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8")
    sub_os = ossys.stdout
    return str(sub_os)

def get_cpu():
    cpu = subprocess.run(["lscpu", "-J"],stdout=subprocess.PIPE,stderr=subprocess.PIPE, encoding="utf-8")
    sub_cpu = cpu.stdout
    dict_cpu = json.loads(sub_cpu)

    #architecture
    arch = str(dict_cpu['lscpu'][0].values())
    sub_arch = arch[13:][:-2].replace(",","").replace("'","")

    #Cores
    get_cores = str(dict_cpu['lscpu'][3].values())
    cores = get_cores[13:][:-2].replace(",","").replace("'","")

    #Model
    get_cpu_mod = str(dict_cpu['lscpu'][10].values())
    model_cpu = get_cpu_mod[13:][:-2].replace(",","").replace("'","")

    #Build a msg
    final_reply = sub_arch+', '+cores+', '+model_cpu+' ...'
    return (final_reply)
#end Commands for raspberry


if __name__ == "__main__":

    BOT_VERSION="1.7.1"
    RELEASE_DATE="2023-05-06"
    version_str=f"Deb-Bot {BOT_VERSION} ({RELEASE_DATE})"
    cf=os.path.join(os.path.dirname(sys.argv[0]),'debbot.conf')
    lf = os.path.join(os.path.dirname(sys.argv[0]), 'debbot.log')
    cmdfile = os.path.join(os.path.dirname(sys.argv[0]), 'irc_commands.json')

    parser = argparse.ArgumentParser(description='Debian News Irc Bot')
    parser.add_argument('-c','--config', type=str,default=cf)
    parser.add_argument('-l', '--logfile', type=str, default=lf)
    parser.add_argument( '--commandfile', type=str, default=cmdfile)
    parser.add_argument('--version', action='version', version=version_str)
    args = parser.parse_args()

    conf=args.config
    logger=get_logger(args.logfile)

    cp=configparser.ConfigParser()
    cp.read(conf)



    ## IRC Config those in the conf file
    
    server=cp['DEFAULT']['server']
    port = int(cp['DEFAULT']['port']) 
    channel = cp['DEFAULT']['channel'] 

    botnick=cp['DEFAULT']['botnick']
    botnickpass=cp['DEFAULT']['botnickpass']
    botpass=cp['DEFAULT']['botpass']
    ipv6=cp['DEFAULT']['ipv6']=='Y'
   

    if ipv6:
        family = socket.AF_INET6
    else:
        family = socket.AF_INET

    check_every_secs=int(cp['DEFAULT']['check_every_secs'])#300
    t=time.time()-10000

    irc = IRC(family,logger)
    #commands
    command_file=args.commandfile

    try:
        commands=irccommands.IRC_commands(command_file,logger)
    except Exception as e:
        logger.error(f"Problem loading commmand file {command_file}: {e}")
        commands = None

    #connection
    irc.connect(server, port, channel, botnick, botnickpass, botpass)
    bot_connection_time=datetime.datetime.utcnow()

    #feed sources
    dsa_url = "https://www.debian.org/security/dsa-long"
    planet_url = "https://planet.debian.org/rss20.xml"
    dsa=getfeeds.GetNewFeeds(dsa_url,"dsa")
    planet=getfeeds.GetNewFeeds(planet_url,"planet")


    loopCondition=True

    #main loop
    while loopCondition:
        irc.settimeout(check_every_secs-(time.time()-t)+1)
        parsed_msg= irc.get_response()
        
        if parsed_msg.command=="PRIVMSG" and channel==parsed_msg.args[0]:
                arg2 = parsed_msg.args[1]
                match arg2:
                    case "hello":
                        irc.send(channel, f"Hello {parsed_msg.get_nick()}!")
                    case "!dsa":
                        print_feeds(channel, getfeeds.get_feeds(dsa_url))
                    case "!planet":
                        print_feeds(channel, getfeeds.get_feeds(planet_url))
                    case "!botversion":
                        irc.send(channel, version_str)
                    case "!botuptime":
                        irc.send(channel, get_bot_uptime())
                if "https://" or "http://" in arg2:
                    url = arg2.split(' ')[0]
                    irc.send(channel, get_url_title(url))
                    

                #Channel Operator Commands:

                adm_list = commands.read_commands(command_file)
                nick_in=arg2.split(' ')[1:]
                possible_nick_adm=parsed_msg.prefix.split(":")[0].split('!')[0]
                if possible_nick_adm in adm_list['admin']['adm']:
                    if len(nick_in)==1:
                        match arg2.split(' ')[0]:
                            case "!op":
                                if len(nick_in)==1:
                                    irc.send_op(channel, nick_in[0])
                    
                        if nick_in[0] != botnick and nick_in[0] not in adm_list['admin']['adm']:
                            match arg2.split(' ')[0]:
                                case "!deop":
                                    if len(nick_in)==1:
                                        irc.send_deop(channel, nick_in[0])
                                case "!kick":
                                    if len(nick_in)==1:
                                        irc.kick_chan(channel, nick_in[0])
                                case "!ban":
                                    if len(nick_in)==1:
                                        irc.kickban_chan(channel, nick_in[0])
                                # commands exit 

                                #case "!exit":
                                #    loopCondition=False


                if commands:
                    cmd=commands.check_and_execute(parsed_msg,botnick)
                    if cmd:
                        irc.send(channel, cmd)
                match arg2.split(' ')[0]:
                    case "!ip":
                        if len(nick_in)==1:
                            ip_command(nick_in[0])
                    case "!dpkg":
                        if len(nick_in)==1:
                            dpkg_command(nick_in[0])

        if time.time()-t > check_every_secs:
            check_news()
            t = time.time()

