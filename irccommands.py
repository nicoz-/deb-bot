#!/usr/bin/env python3

import json,re

#this class allows you to add some simple commands using a json command file

class IRC_commands:
    def __init__(self,command_file,logger=None):
        self._command_file = command_file
        self._commands=self.read_commands(command_file)
        self._pattern = re.compile(r"!([a-zA-Z0-9]+)$")
        self._logger=logger

    def read_commands(self,fn):
        with open(fn, "r") as read_file:
            data = json.load(read_file)
        return data

    def execute(self,key,command,nick):
        sc=""
        if key=="reload":
            if (nick in command.get("allow")) or ("*" in command.get("allow")):
                self._commands = self.read_commands(self._command_file)
                sc="Command file reloaded"
        else:
            sc=command.get("show")
        return sc

    # def check_and_execute(self, text):
    #     result=None
    #     cmd = self.check(text)
    #     if cmd:
    #         result=self.execute(cmd)
    #     return result

    def check_and_execute(self, parsed_message,botnick):
        result=None
        key,cmd = self.check(parsed_message,botnick)
        if cmd:
            result=self.execute(key,cmd,parsed_message.get_nick())
        return result

    def check(self, parsed_message,botnick):
        key,cmd = None,None
        if parsed_message.command =="PRIVMSG" and botnick!=parsed_message.get_nick():
            s=parsed_message.args[1]
            # pattern=re.compile(r"(.*)\!([a-z]+)$")
            m = self._pattern.match(s)
            if m!=None and len(m.groups())==1:
                pcmd=m.group(1)
                self.log(f"Check command candidate pattern found: {pcmd}")
                if pcmd in self._commands:
                    key,cmd=pcmd,self._commands[pcmd] #self._commands.get(pcmd)

        return key,cmd

    def log(self,msg):
        if self._logger:
            self._logger.info(msg)
        else:
            print(msg)

# if __name__ == '__main__':
    # commands={"hello":{"show":"Hello guys!"},"hallo":{"show":"Hallo guys!"},"smile":{"show":":-)"}}
    # with open('test.json', 'w') as fout:
    #     json.dump(commands, fout)
    # with open("irc_commands.json", "r") as read_file:
    #     data = json.load(read_file)
    # print(data)
    # c=IRC_commands("irc_commands.json",None)
    # print(c.check("dsfsaf!hello"))
